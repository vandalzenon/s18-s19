//[SECTION] Objects

//Objects -> is a collection of related data and/or functionalities. the purpose of an object is not only to store multiple  data-sets but also to represent real world objects.

//SYNTAX: 
// let/const variableName = {
// 	key/property: value
// }

//Note: Information stored in objects are represented in key:value pairing. 

//lets use this example to describe a real-world item/object
let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: '1999',
    price: 1000
}; //'key' -> is also mostly referred to as a 'property' of an object. 

console.log(cellphone);
console.log(typeof cellphone);

//How to store multiple objects, 

//you can use an array struture to store them.
let users = [
    {
        name: 'Anna',
        age: '23'
    },
    {
        name: 'Nicole',
        age: '18'
    },
    {
        name: 'Smith',
        age: '42'
    },
    {
        name: 'Pam',
        age: '13'
    },
    {
        name: 'Anderson',
        age: '26'
    }
];

console.log(users);
//now there is an alternative way when displaying values of an object collection
console.table(users);

//Complex examples of JS objects

//NOTE: Different data types may be stored in an object's property creating more complex data structures

//When using JS Objects, you can also insert 'Methods'.

//Methods -> are useful for creating reusable functions that can perform tasks related to an object. 
let friend = {
    //properties
    firstName: 'Joe', //String or numbers
    lastName: 'Smith',
    isSingle: true, //boolean
    emails: ['joesmith@gmail.com', 'jsmith@company.com'], //array
    address: { //Objects
        city: 'Austin',
        state: 'Texas',
        country: 'USA'
    },
    //methods 
    introduce: function () {
        console.log('Hello Meet my new Friend');
    },
    advice: function () {
        console.log('Give friend an advice to move on');
    },
    wingman: function () {
        console.log('Hype friend when meeting new people');
    }
}
console.log(friend);
